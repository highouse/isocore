# IsoCore

![IsoCoreBoardTopSide](/Images/IsoCore_Assembled_Top_1.jpg)

The IsoCore is a derivative of the [pyboard](https://github.com/micropython/pyboard). The IsoCore also employs an STM32F405RGT6 with support for a �USB connection and �SD card, but without the accelerometer of the original pyboard.

The IsoCore breaks out I/O to headers by Port as well as to a PCIeX1 card-edge connector, tailored for a custom ecosystem.

It is intended (but there is no absolute need) to run [microPython](www.micropython.org). As of 1.9.4, the stock image for the pyboard will work with the IsoCore after the 560R mod noted below.

It was developed primarily for this author's own personal ecosystem as a custom development tool.

---

## Rev SEPT18 Eratta:

* LED silkscreen matches the markings on the component undersides. This may cause confusion with an assembly house.
* With the AP2112K-3.3 Regulator, DNP D5 if using a 12V supply through the edge connector.
* If using this board stand-alone with USB, fit a 560R resistor between PA9 and VUSB for USB detection by the board. 
   (An example using a through hole resistor connected to vias can be seen above).

---

## Designer's General Notes

Several odd practices have been incorporated because the author is experimenting:

* USB is run both to the connector and to the PCIeX1 card-edge connector, thence onto *gasp* a ribbon cable bus.
   Jumpers R14e and R15e may be removed to route solely to the uUSB connector

* Vias poke through the silkscreen on the back: It's for information, not for production - it suits this use case.

* The PCIeX1 card-edge connector is bastardized for use here. While some pins actually line up with the standard, most do not.
    
    The PCIeX1 connector is common and likely to be available for many years at mainline distributors. 
    
    After that, available by desoldering-iron.
    
    It's also a rather pleasant and satisfying feeling to use this kind of connector.


Several units have been built and meet the author's uses:

* One batch of bare boards was made at [OSHPark](https://oshpark.com/)

* One batch was assembled at [MacroFab](https://macrofab.com/)

No warranty or fitness for a particular use is offered or implied.

---

## License

This project is a derivative of the [pyboard, originally developed by Damien P. George](https://github.com/micropython/pyboard) with changes made.

No endorsement of this project has been made or asked for.

(Frankly I just made this for my own ecosystem of things in my home lab).

Copyright (c) 2018, Benjamin Bloss

This work is licensed under the Creative Commons Attribution 3.0
Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/3.0/ or send a letter to
Creative Commons, 444 Castro Street, Suite 900, Mountain View,
California, 94041, USA.

---